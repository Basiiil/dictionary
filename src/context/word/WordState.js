import React,{useReducer} from "react";
import axios from "axios";
import WordContext from './WordContext';
import WordReducer from './WordReducer';
import {
  SHOW_WORDS,
  WORD_ACT,
  ERROR_CONNECTION,
  GET_WORD,
  SEARCH_WORD,
  SET_STATUS
} from '../types';


const WordState = props => {
  const initialState = {
    message: '',
    words: [],
    word: {},
    status: '',
    result:[]
    
  }

  const [state, dispatch] = useReducer(WordReducer, initialState);

  // create words
  const createWords = async (word) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }
    try {
      const sendData = {};
      Object.entries(word)
        .filter(([key, value]) => value && value.length > 0)
        .forEach(([key, value]) => {
          sendData[key] = value;
      })
      
      const res = await axios.post(`http://alp.thescenius.com/api/v1/words`, sendData, config);
      if (res.data.status) {
        dispatch({
          type: WORD_ACT,
          status: res.data.status,
          message: 'کلمه ی جدید اضافه شد'
        });
        setStatus();
      }
    } catch (err) {
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.errors.source
      })
    }

  }


  // show all words
  const allWords = async () => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }

    const res = await axios.get(`http://alp.thescenius.com/api/v1/words`, config);
    dispatch({
      type: SHOW_WORDS,
      words: res.data.body
    });

  }

  // show a word
  const getWord = async (id) => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }

    const res = await axios.get(`http://alp.thescenius.com/api/v1/words/${id}`, config);
    dispatch({
      type: GET_WORD,
      word: res.data.body
    });

  }

  // search the word
  const searchWord = async (title) => {

    try{
      const res = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${title}`);
      // console.log(res.data);
    dispatch({
      type: SEARCH_WORD,
      result: res.data
    });
    } catch (err) {
      // dispatch({
      //   type: ERROR_CONNECTION,
      //   phonetics: 'No Result',
      //   meanings: 'No Result'
      // })
      // if (err.response.status === 404) {
      //   // console.log('heyyyy')
      //   // dispatch({
      //   //     type: ERROR_CONNECTION,
      //   //     phonetics: 'No Result',
      //   //     meanings: 'No Result'
      //   //   })
      //   dispatch({
      //     type: ERROR_CONNECTION,
      //     phonetics: ['No Result'],
      //       meanings: ['No Result']
      //   });
      // }
      // console.log(err.response)
    }
  }

  // update a word
  const updateWord = async (id, word) => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }

    const sendData = {};
    Object.entries(word)
      .filter(([key, value]) => value && value.length > 0)
      .forEach(([key, value]) => {
        sendData[key] = value;
      })
    
    // console.log(sendData);

    const res = await axios.put(`http://alp.thescenius.com/api/v1/words/${id}`, sendData, config);
    allWords()
  }

  // remove word
  const removeWord = async (id) => {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }

    const res = await axios.delete(`http://alp.thescenius.com/api/v1/words/${id}`, config);
    allWords()

  }

  const setStatus = () => dispatch({ type: SET_STATUS });

  return (
    <WordContext.Provider
      value={{
        message: state.message,
        result: state.result,
        words: state.words,
        word: state.word,
        status: state.status,
        createWords,
        allWords,
        removeWord,
        getWord,
        updateWord,
        searchWord
      }}
    >
      {props.children}
    </WordContext.Provider>
  );

}

export default WordState