import {
  SHOW_WORDS,
  WORD_ACT,
  ERROR_CONNECTION,
  GET_WORD,
  SEARCH_WORD,
  SET_STATUS

} from '../types';

export default (state, action) => {
  switch (action.type) {
    case WORD_ACT:
      return {
        ...state,
        status: action.status,
        message: action.message
      }
    case SHOW_WORDS:
      return {
        ...state,
        words: action.words
      }
    case GET_WORD:
      return {
        ...state,
        word: action.word
      }
    case SEARCH_WORD:
      return {
        ...state,
        result: action.result,
      }
    case ERROR_CONNECTION:
      return {
        ...state,
        message: action.message,
        status: false
      }
    
      case SET_STATUS:
        return {
          ...state,
          status: false
        }
    
    default:
      return state;
  }
}