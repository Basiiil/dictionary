import React, { useReducer,useContext } from "react";
import axios from "axios";
import OuthContext from './OuthContext';
import OuthReducer from './OuthReducer';
import {
  USER_ACT,
  ERROR_CONNECTION,
  SET_STATUS
} from '../types';

const OuthState = props => {
  const initialState = {
    message: '',
    status: '',
    emailErorr: '',
    codeErorr: '',
    nameErorr: '',
  }

  const [state, dispatch] = useReducer(OuthReducer, initialState);


  // Register user
  const userRegister = async (email) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    try {
      
      const res = await axios.post(`http://alp.thescenius.com/api/v1/auth/user/register`, { email }, config);
      console.log(res);

      if (res.data.status) {
        dispatch({
          type: USER_ACT,
          status: res.data.status,
          message: res.data.message
        });
        setStatus();
      }
    } catch (err) {
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.errors.email
      })
    }

  }


  // Verification user
  const userVerification = async (verifi) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    try {
      const res = await axios.post(`http://alp.thescenius.com/api/v1/auth/user/account-verification`, verifi, config);

      if (res.data.status) {
        dispatch({
          type: USER_ACT,
          status: res.data.status,
          message: 'ثبت نام با موفقیت انجام شد'
        });
        setStatus();
      }
    } catch (err) {
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.errors
      })
    }
    
  }


  // forget password
  const forgetPass = async (email) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    try {
      const res = await axios.post(`http://alp.thescenius.com/api/v1/auth/user/forgot-password`, { email }, config);

      console.log(res);

      if (res.data.status) {
        dispatch({
          type: USER_ACT,
          status: res.data.status,
          message: res.data.message
        });
        setStatus();
      }
    } catch (err) {
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.errors.email
      })
    }

  }

  // forget completion password
  const forgetPassCompletion = async (completion) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    try {
      const res = await axios.post(`http://alp.thescenius.com/api/v1/auth/user/forgot-password-completion`, completion, config);

      if (res.data.status) {
        dispatch({
          type: USER_ACT,
          status: res.data.status,
          message: res.data.message
        });
        setStatus();
      }
    } catch (err) {
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.errors.code
      })
    }

  }

  // user Login
  const userLogin = async (login) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    try {
      const res = await axios.post(`http://alp.thescenius.com/api/v1/auth/user/login`, login, config);
      localStorage.setItem('token', res.data.body.access_token);

      dispatch({
        type: USER_ACT,
        status: res.data.status
      });
    } catch (err) {
      console.log(err.response);
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.message
      })
    }

  }

  // user change pass
  const userChangePass = async (changePass) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    }
    try {
      const res = await axios.post(`http://alp.thescenius.com/api/v1/auth/user/change-password`, changePass, config);
      if (res.data.status) {
        dispatch({
          type: USER_ACT,
          status: res.data.status,
          message: res.data.message
        });
        setStatus();
      }
    } catch (err) {
      dispatch({
        type: ERROR_CONNECTION,
        message: err.response.data.errors.password
      })
    }
  }


  const setStatus = () => dispatch({ type: SET_STATUS });

  return (
    <OuthContext.Provider
      value={{
        message: state.message,
        codeErorr: state.codeErorr,
        emailErorr: state.emailErorr,
        nameErorr: state.nameErorr,
        status: state.status,
        userRegister,
        userVerification,
        forgetPass,
        forgetPassCompletion,
        userLogin,
        userChangePass,
        setStatus
      }}
    >
      {props.children}
    </OuthContext.Provider>
  )

}

export default OuthState