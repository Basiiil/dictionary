import {
  USER_ACT,
  ERROR_CONNECTION,
  SET_STATUS,
} from '../types';

export default (state, action) => {
  switch (action.type) {
    case USER_ACT:
      return {
        ...state,
        message: action.message,
        status: action.status,
      };
    
    case ERROR_CONNECTION:
      return {
        ...state,
        message: action.message,
        status: false
      };
    
    case SET_STATUS:
      return {
        ...state,
        status: false
      }

    default:
      return state;
  }
}