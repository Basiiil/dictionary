import React from 'react';
import { useParams } from 'react-router-dom';
import './Search.css'

function Urban() {
  const { title } = useParams();
  return (
    <div>
      <h1 className='title'>Urban Dictionary</h1>
      <div className='search-into-site'>
        <iframe className="search-box" src={`https://www.urbandictionary.com/define.php?term=${title}`} ></iframe>

      </div>
    </div>
  );
}

export default Urban