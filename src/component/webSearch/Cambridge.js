import React from 'react';
import { useParams } from 'react-router-dom';
import './Search.css'

function Cambridge() {
  const { title } = useParams();
  return (
    <div>
      <h1 className='title'>Cambridge Dictionary</h1>
      <div className='search-into-site'>
        <iframe className="search-box" src={`https://dictionary.cambridge.org/dictionary/english/${title}`} ></iframe>

      </div>
    </div>
  );
}

export default Cambridge