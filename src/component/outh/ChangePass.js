import React, { useState, useContext,useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { passSchema } from '../validations/UserPassword';
import UseInput from '../reusable/UseInput';
import UseButton from '../reusable/UseButton';
import OuthContext from '../../context/outh/OuthContext';
import AlertContext from '../../context/alert/AlertContext';

const ChangePass = () => {
  const { userChangePass, status, message } = useContext(OuthContext);
  // const history = useHistory();
  const location = useLocation();
  const alertContext = useContext(AlertContext);
  const navigate = useNavigate();
  const [changePass, setChangePass] = useState({
    password: "",
    new_password: ""
  });

  const changePassSubmit = async (e) => {
    e.preventDefault();
    let formData = {
      password: changePass.password,
      newpass: changePass.new_password,
      
    };
    const isValid = await passSchema.isValid(formData);
    {isValid && 
      userChangePass(changePass);
    };
    
  }

  const changePassChange = (e) => {
    const { name, value } = e.target;
    setChangePass(prev => ({
      ...prev,
      [name]: value
    }));
  };

  useEffect(() => {
    if (!localStorage.getItem('token')) {
      navigate('/login');
    }

    if (status !== '') {
      if (status) {
        alertContext.setAlert(`${message}`, 'secondary');
        localStorage.clear()
        navigate('/login');
        
      } else {
        alertContext.setAlert(`${message}`, 'secondary');
      }
    }

  }, [message]);

  return (

    <form className='form' onSubmit={changePassSubmit}>
      <h1>Change password</h1>

      <UseInput
        label='Password'
        type="password"
        placeholder="Enter Password"
        onChange={changePassChange}
        value={changePass.password}
        name='password'
        required={true}
      />

      <UseInput
        label='New Password'
        type="password"
        placeholder="Enter New Password"
        onChange={changePassChange}
        value={changePass.new_password}
        name='new_password'
        required={true}
      />

      <UseButton text='Submit' />

    </form>


  );
};

export default ChangePass