import React, { useState, useContext,useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { emailSchema } from '../validations/UserEmail';
import UseInput from '../reusable/UseInput';
import UseButton from '../reusable/UseButton';
import UseSocial from '../reusable/UseSocial';
import OuthContext from '../../context/outh/OuthContext';
import AlertContext from '../../context/alert/AlertContext';
import googleImage from '../../assets/icons/google.png'
import twitterImage from '../../assets/icons/twitter.png'
import facebookImage from '../../assets/icons/facebook.png'


const Register = () => {
  const { userRegister, status, message } = useContext(OuthContext);
  const alertContext = useContext(AlertContext);
  const [email, setEmail] = useState('');
  const navigate = useNavigate()

  const formSubmit = async (e) => {
    e.preventDefault();
    let formData = {
      email: email
    };
    const isValid = await emailSchema.isValid(formData);
    {isValid &&
      userRegister(email)
    };

  }
  // console.log(isValid);
  
  const emailChange = (e) => setEmail(e.target.value);
  
  useEffect(() => {

    if (status !== '') {
      if (status) {
        alertContext.setAlert(`${message}`, 'secondary');
        navigate('/verification');

      } else {
        alertContext.setAlert(`${message}`, 'secondary');
      }
    }
  }, [message]);

  return (

    <form className='form' onSubmit={formSubmit}>
      <h1>Register</h1>

      <UseInput
        label='Email'
        type='email'
        placeholder='Enter Your Email'
        onChange={emailChange}
        value={email}
        name='email'
        required={true}
      />

      <UseButton text='Submit' />

      <div className="outher-forms">
        <p>Have an account?</p>
        <Link
          to='/login'
          className='form__link'
        >  Login
        </Link>

      </div>
         
      <UseSocial google={googleImage} twitter={twitterImage} facebook={facebookImage} />
    </form>

  );
};

export default Register