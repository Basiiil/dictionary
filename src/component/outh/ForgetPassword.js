import React, { useContext, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { emailSchema } from '../validations/UserEmail';
import UseInput from '../reusable/UseInput';
import UseButton from '../reusable/UseButton';
import OuthContext from '../../context/outh/OuthContext';
import AlertContext from '../../context/alert/AlertContext';

const ForgetPassword = () => {
  const navigate = useNavigate()
  const { forgetPass, status, message } = useContext(OuthContext);
  const alertContext = useContext(AlertContext);
  const [email, setEmail] = useState('');

  const formSubmit = async(e) => {
    e.preventDefault();
    let formData = {
      email: email
    };
    const isValid = await emailSchema.isValid(formData);
    {
      isValid &&
      forgetPass(email);
    };
    
  }

  const emailChange = (e) => setEmail(e.target.value);

  useEffect(() => {

    if (status !== '') {
      if (status) {
        alertContext.setAlert(`${message}`, 'secondary');
        navigate('/forgetPassCompletion');

      } else {
        alertContext.setAlert(`${message}`, 'secondary');
      }
    }

  }, [message]);

  return (

    <form className='form' onSubmit={formSubmit}>
      <h1>Forget Password</h1>

      <UseInput
        label='Email'
        type="email"
        placeholder="Enter email"
        onChange={emailChange}
        value={email}
        name='email'
        required={true}
      />

      <UseButton text='Submit' />

    </form>
  );
};

export default ForgetPassword