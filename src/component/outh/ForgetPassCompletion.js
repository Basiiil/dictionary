import React, { useState, useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { forgetSchema } from '../validations/Forget';
import UseInput from '../reusable/UseInput';
import UseButton from '../reusable/UseButton';
import OuthContext from '../../context/outh/OuthContext';
import AlertContext from '../../context/alert/AlertContext';


const ForgetPassCompletion = () => {
  const { forgetPassCompletion, status, message } = useContext(OuthContext);
  const alertContext = useContext(AlertContext);
  const navigate = useNavigate()

  const [completion, setCompletion] = useState({
    email: "",
    new_password: "",
    code: ""
  });

  const completionSubmit = async (e) => {
    e.preventDefault();
    let formData = {
      email: completion.email,
      password: completion.new_password,
      code: completion.code
    };
    const isValid = await forgetSchema.isValid(formData);
    {isValid && 
      forgetPassCompletion(completion);
    };
    
  }

  const passChange = (e) => {
    const { name, value } = e.target;
    setCompletion(prev => ({
      ...prev,
      [name]: value
    }));
  };

  useEffect(() => {

    if (status !== '') {
      if (status) {

        alertContext.setAlert(`${message}`, 'secondary');
        navigate('/login');

      } else {
        alertContext.setAlert(`${message}`, 'secondary');
      }
    }
  }, [message]);

  return (
    
    <form className='form' onSubmit={completionSubmit}>
      <h1>Forget Password</h1>

      <UseInput
        label='Email'
        type="email"
        placeholder="Enter email"
        onChange={passChange}
        value={completion.email}
        name='email'
        required={true}
      />

      <UseInput
        label='Password'
        type="password"
        placeholder="Enter New Password"
        onChange={passChange}
        value={completion.new_password}
        name='new_password'
        required={true}
      />

      <UseInput
        label='Code'
        type="text"
        placeholder="Enter Code"
        onChange={passChange}
        value={completion.code}
        name='code'
        required={true}
      />

      <UseButton text='Submit' />

    </form>

  );
};

export default ForgetPassCompletion