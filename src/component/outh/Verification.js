import React, { useState, useContext,useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { verifySchema } from '../validations/UserVerify';
import UseButton from '../reusable/UseButton';
import UseInput from '../reusable/UseInput';
import { Form, Button, } from 'react-bootstrap';
import OuthContext from '../../context/outh/OuthContext';
import AlertContext from '../../context/alert/AlertContext';


const Verification = () => {
  const { userVerification, status, message } = useContext(OuthContext);
  const alertContext = useContext(AlertContext);
  const navigate = useNavigate();
  const [verifi, setVerifi] = useState({
    email: "",
    code: "",
    password: "",
    name: ""
  });

  const verifiSubmit = async (e) => {
    e.preventDefault();
    let formData = {
      email: verifi.email,
      code: verifi.code,
      password: verifi.password,
      name: verifi.name
    };
    const isValid = await verifySchema.isValid(formData);
    {isValid && 
      userVerification(verifi)
    };
  };

  const verifiChange = (e) => {
    const { name, value } = e.target;
    setVerifi(prev => ({
      ...prev,
      [name]: value
    }));
  };

  useEffect(() => {
    if (status !== '') {
      if (status) {
        alertContext.setAlert(`ثبت نام با موفقیت انجام شد`, 'secondary');
        navigate('/login');
      } else {
        {
          message.code && 
            alertContext.setAlert(`${message.code}`, 'secondary');
        }
        {
          message.email && 
            alertContext.setAlert(`${message.email}`, 'secondary');
        }
        {
          message.name && 
            alertContext.setAlert(`${message.name}`, 'secondary');
        }
      }
    }
  }, [message]);


  return (
    
    <form className='form' onSubmit={verifiSubmit}>
      <h1>Verification</h1>

      <UseInput
        label='Email'
        type='email'
        placeholder='Enter Your Email'
        onChange={verifiChange}
        value={verifi.email}
        name='email'
        required={true}
      />

      <UseInput
        label='Code'
        type="text"
        placeholder="Enter Code"
        onChange={verifiChange}
        value={verifi.code}
        name='code'
        required={true}
      />
      <UseInput
        label='Password'
        type='password'
        placeholder='Enter Your Password'
        onChange={verifiChange}
        value={verifi.password}
        name='password'
        required={true}
      />
      <UseInput
        label='Name'
        type="text"
        placeholder="Enter Name"
        onChange={verifiChange}
        value={verifi.name}
        name='name'
        required={true}
      />

      <UseButton text='login' />
         
    </form>
  );
};


export default Verification