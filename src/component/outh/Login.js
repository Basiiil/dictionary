import React, { useState, useContext,useEffect } from 'react';
import { useNavigate,Link  } from 'react-router-dom';
import UseButton from '../reusable/UseButton';
import { loginSchema } from '../validations/UserLogin';
import UseInput from '../reusable/UseInput';
import UseSocial from '../reusable/UseSocial';
import OuthContext from '../../context/outh/OuthContext';
import AlertContext from '../../context/alert/AlertContext';
import WordContext from '../../context/word/WordContext'; 
import googleImage from '../../assets/icons/google.png'
import twitterImage from '../../assets/icons/twitter.png'
import facebookImage from '../../assets/icons/facebook.png'

const Login = () => {
  const { userLogin, status, message } = useContext(OuthContext);
  const { allWords } = useContext(WordContext);
  const alertContext = useContext(AlertContext);
  const navigate = useNavigate();
  const [login, setLogin] = useState({
    email: "",
    password: ""
  });

  const loginSubmit = async (e) => {
    e.preventDefault();
    let formData = {
      email: login.email,
      password: login.password
    };
    const isValid = await loginSchema.isValid(formData);
    {
      isValid &&
      userLogin(login)
    };
      
  };

  const loginChange = (e) => {
    const { name, value } = e.target;
    setLogin(prev => ({
      ...prev,
      [name]: value
    }));
  };

  useEffect(() => {

    if (status !== '') {
      if (status) {
        navigate('/wordsList');
        // window.location.reload();
      } else {
        alertContext.setAlert(`${message}`, 'secondary');
      }
    }

  }, [message]);

  return (
   
    <form className='form' onSubmit={loginSubmit}>
      <h1>Login</h1>

      <UseInput
        label='Email'
        type='email'
        placeholder='Enter Your Email'
        onChange={loginChange}
        value={login.email}
        name='email'
        required={true}
      />

      <UseInput
        label='Password'
        type='password'
        placeholder='Enter Your Password'
        onChange={loginChange}
        value={login.password}
        name='password'
        required={true}
      />

      <UseButton text='login' />

      <div className="outher-forms">
        <p>Don't have an account?</p>
        <Link
          to='/register'
          className='form__link'
        >  Register
        </Link>

        <Link to='/forgetPassword'
          className='form__link forgot-pass'
        >Forgot password</Link>
      </div>
         
      <UseSocial google={googleImage} twitter={twitterImage} facebook={facebookImage} />
    </form>
  )
};


export default Login