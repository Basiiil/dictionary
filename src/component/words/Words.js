import React, { useState, useContext, useEffect } from 'react';
import './WordList.css';
import { useNavigate  } from 'react-router-dom';
import WordList from './WordList';
import ListSlider from '../layout/ListSlider';
import WordContext from '../../context/word/WordContext'; 
import { Table } from 'react-bootstrap';

const Words = () => {
  const { allWords,searchWord, words, getWord, removeWord,result,meanings,resultWord } = useContext(WordContext);
  const navigate = useNavigate();
  
  useEffect(() => {
    allWords();
    // searchWord(words.title);
    // console.log(words);
    
    if (!localStorage.getItem('token')) {
      navigate('/login');
    } 
  },[])

  return (

    <div>
      
      {words.map(word => (
        <div key={word.id}>
          <section className="section-A">  
            <WordList
              word={word}
              searchWord={searchWord}
              showWord={getWord}
              deleteWord={removeWord}
              result={result}
              meanings={meanings}
              resultWord={resultWord}
              />
              <ListSlider title={word.title} />
           </section> 
        </div>
      ))}
        
    </div>
  );
};


export default Words