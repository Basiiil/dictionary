import React, { useEffect,useContext,useState } from 'react';
import ReadMore from '../reusable/ReadMore';
import ReactReadMoreReadLess from "react-read-more-read-less";
import { Link, useNavigate } from 'react-router-dom';
import editImage from '../../assets/icons/edit.png';
import deleteImage from '../../assets/icons/delete.png';
import WordContext from "../../context/word/WordContext";


// const WordList = () => {
  const WordList = ({word: { id,title,body } ,deleteWord,showWord,searchWord,result}) => {
    const navigate = useNavigate();
    
    useEffect(() => {
      searchWord(title);
      setDefin(result);
      
      console.log(title);
      console.log(result);
    }, []);

    
// setDefin(result)
      // console.log(result);
    const [defin, setDefin] = useState([]);
    console.log(defin);
    // console.log(title);

    // searchWord(title);


    // console.log('hi');
    //   console.log(result);

    // if (resultWord === title) {
    //   searchWord(title);
    // }

    // console.log(resultWord);
    // searchWord(title);
    // console.log(meanings)
    // console.log(title)
    // const [meaning, setMeaning] = useState({
    //   meanings: meanings
    // });

  return (

    <div className="section-info">
        
      <div>
        <span className="circle"></span>
        <p>Title</p>
        <div className="section-info__box card">
          {/* <ReadMore text={title} limit={15} /> */}
          <ReactReadMoreReadLess
            readMoreClassName='textmore'
            readLessClassName='textmore'
            charLimit={15}
            readMoreText={"Read more ▼"}
            readLessText={"Read less ▲"}
          >
            {title}
            
          </ReactReadMoreReadLess>
          
          <button className='section-info__caed-btn'
            onClick={
              () => {
                showWord(id);
                searchWord(title)
                navigate(`show/${title}`)
              }}
          >
            Show
          </button>
        </div>
      </div>
      <div>
        <p>Priority</p>
        <div className="section-info__box priority">
          top priority
        </div>
      </div>
      <div className="mr-top">
        <p>Description</p>
        <div className="section-info__box description">

        {/* <ReactReadMoreReadLess
            readMoreClassName='textmore'
            readLessClassName='textmore'
            charLimit={15}
            readMoreText={"Read more ▼"}
            readLessText={"Read less ▲"}
          >
            {body}
            
          </ReactReadMoreReadLess> */}

          {/* {body ? body : meanings.map((mean,index) => (
            <div key={index}>
              {mean.definitions.map((def,index) => (
                <p key={index}>{ def.definition }</p>
              ))}
            </div>
          )) } */}
          {body}
          
        {/* {title === res.word && */}
          {!body && defin.map((res, index) => (
        <div key={index}>

        {
          res.meanings.map((mean, index) => (
          <span key={index}>

            {mean.definitions.map((defin, index) => (
              <div key={index} className="form__show-detail">
                <label htmlFor="phonetic">definitions:</label>

                {defin.definition &&
                  <p>{defin.definition}</p>
                }
                
              </div>
            ))}
            
            </span>
          ))}

        </div>
      ))}

          








          {/* {resultWord === title &&  */}
          
          {/* {!body && 
            meanings.map((mean,index) => (
              <div key={index}>
                {mean.definitions && mean.definitions.map((def,index) => (
                  <p key={index}>{ def.definition }</p>
                ))}
              </div>
            ))
            } */}
          {/* {!body && 
            meanings.map((mean,index) => (
              <div key={index}>
                {mean.definitions && mean.definitions.map((def,index) => (
                  <p key={index}>{ def.definition }</p>
                ))}
              </div>
            ))
            } */}
        </div>
      </div>
      <div className="mr-top">
        <p>Operation</p>
        <div className="section-info__box operation">
          <img
            src={editImage}
            alt="edit"
            className='imageSize edit'
            onClick={() => {
              showWord(id);
              navigate(`edit/${id}`)
            }}
          />
          <img
            src={deleteImage}
            alt="delete"
            className='imageSize delete'
            onClick={() => deleteWord(id)}
          />
        </div>
      </div>
      
    

      {/* <tr>
      <td>{id}</td>
      <td>{title}</td>
      <td>{body}</td>
      <td className='list-btn'>
        
        <Link className='btn btn-success link-btn' to={`https://www.urbandictionary.com/define.php?term=${title}`} >Search</Link>
        
        <Button variant="primary" className='m-1'
          onClick={() => {
            showWord(id);
            navigate(`show/${id}`)
          }} >
          Show
        </Button>
        <Button variant="info" className='m-1'
          onClick={() => {
            showWord(id);
            navigate(`edit/${id}`)
          }} >
          edit
        </Button>
        <Button variant="light" className='m-1' onClick={() => deleteWord(id)} >Delete</Button>
        
      </td>
    </tr> */}
    </div>
  );
};


export default WordList