import React, {useState, useContext, useEffect} from "react";
import {useNavigate, Link} from "react-router-dom";
import { Howl } from 'howler';
import UseInput from '../reusable/UseInput';
import UseButton from '../reusable/UseButton';
import WordContext from "../../context/word/WordContext";
import speaker from '../../assets/icons/speaker.png';

function Show() {
  const navigate = useNavigate();
  const { word,result, phonetics, meanings, message } = useContext(WordContext);

  // play sound
  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true
    })
    sound.play();
  }

  // words
  useEffect(() => {
    if (word) {
      setWordEdit({
        title: word.title,
        body: word.body,
        source: word.source
      });
    }
          
    if (!localStorage.getItem('token')) {
      navigate('/login');
    }
  }, [word]);


  useEffect(() => {
    if (result) {
      setSearchResult({
        result:result
      });
    }
  }, [result]);

  const [wordEdit, setWordEdit] = useState({
    title: word.title,
    body: word.body,
    source: word.source
  });

  const [searchResult, setSearchResult] = useState({
    result: result
  })
  
  return (

    <form className='form' >
      <h1>Show this word</h1>
      <div className="form__show-detail">
        <label htmlFor="title">Title:</label>
        <h4 name='title'>{wordEdit.title}</h4>
      </div>
      

      {wordEdit.body &&
        <div className="form__show-detail">
          <label htmlFor="body">body:</label>
          <h4 name='body'>{wordEdit.body}</h4>
        </div>
      }
      {wordEdit.source &&
        <div className="form__show-detail">
          <label htmlFor="source">Source:</label>
          <h4 name='source'>{wordEdit.source}</h4>
        </div>
      }


      {searchResult.result.map((res, index) => (
        <div key={index}>

        {
          res.meanings.map((mean, index) => (
          <div key={index}>

            <div className="form__show-detail">
              <label htmlFor="partOfSpeech">PartOfSpeech:</label>
              <h4>{mean.partOfSpeech}</h4>
            </div>

            {mean.definitions.map((defin, index) => (
              <div key={index} className="form__show-detail">
                <label htmlFor="phonetic">definitions:</label>

                {defin.definition &&
                  <h4>{defin.definition}</h4>
                }
                {defin.example &&
                  <div>
                    <label htmlFor="example">example:</label>
                    <h4>{defin.example}</h4>
                  </div>
                }
            
              </div>
            ))}
              
            {res.phonetics.map((phonetic, index) => (
              <div key={index} className="form__show-detail">
                <label htmlFor="phonetic">Phonetic:</label>

                {phonetic.text &&
                  <h4>{phonetic.text}</h4>
                }
                {phonetic.audio &&
                  <img
                    src={speaker}
                    alt="speaker"
                    className="speaker"
                    onClick={() => SoundPlay(phonetic.audio)}
                  />
                }
                
              </div>
            ))}
            
            </div>
          ))}

        </div>
      ))}

      <Link
        to={`/wordsList`}
        className='cancel-btn'
      >
        Go Back
      </Link>

    </form>


  );
};

export default Show