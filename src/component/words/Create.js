import React, { useState, useContext,useEffect } from 'react';
import { useNavigate  } from 'react-router-dom';
import { wordSchema } from '../validations/Word';
import UseButton from '../reusable/UseButton';
import UseInput from '../reusable/UseInput';
import WordContext from '../../context/word/WordContext'; 
import AlertContext from '../../context/alert/AlertContext';


const Create = () => {
  const navigate = useNavigate()
  const { createWords, status, message } = useContext(WordContext);
  const alertContext = useContext(AlertContext);

  const [word, setWord] = useState({
    title: '',
    body: '',
    source: ''
  });

  const handleSubmit = async(e) => {
    e.preventDefault();
    let formData = {
      title: word.title,
      body: word.body,
      source: word.source
    };
    const isValid = await wordSchema.isValid(formData);

    
    {
      isValid && 
      createWords(word);
    };

  };

  const createChange = (e) => {
    const { name, value } = e.target;
    setWord(prev => ({
      ...prev,
      [name]: value,
    }));
  };

  useEffect(() => {
    if (!localStorage.getItem('token')) {
      navigate('/login');
    }

    if (status !== '') {
      if (status) {
        alertContext.setAlert(`${message}`, 'secondary');
        navigate('/wordsList');
      } else {
        alertContext.setAlert(`${message}`, 'secondary');
      }
    }

  }, [message]);



  return (

    <form className='form' onSubmit={handleSubmit}>
      <h1>Create New word</h1>

      <UseInput
        label='Title'
        type="text"
        placeholder="Enter Title"
        onChange={createChange}
        value={word.title}
        name='title'
        required={true}
      />

      <UseInput
        label='Body'
        type="text"
        placeholder="Enter Body"
        onChange={createChange}
        value={word.body}
        name='body'
        required={false}
      />

      <UseInput
        label='Source'
        type="text"
        placeholder="Enter Source"
        onChange={createChange}
        value={word.source}
        name='source'
        required={false}
      />

      <UseButton text='submit' />

    </form>

  );
};


export default Create