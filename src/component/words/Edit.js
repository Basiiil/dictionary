import React, { useState, useContext, useEffect } from "react";
import { useNavigate, Link, useParams } from "react-router-dom";
import { wordSchema } from "../validations/Word";
import UseInput from "../reusable/UseInput";
import UseButton from "../reusable/UseButton";
import WordContext from "../../context/word/WordContext";

const Edit = () => {
  const navigate = useNavigate();
  const {word, updateWord} = useContext(WordContext);
  const { id } = useParams();

  useEffect(() => {
    if (word) {
      setWordEdit({
        title: word.title,
        body: word.body,
        source: word.source
      });
    }
          
    if (!localStorage.getItem('token')) {
      navigate('/login');
    }
  }, [word])

  const [wordEdit, setWordEdit] = useState({
    title: '',
    body: '',
    source: ''
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    let formData = {
      title: wordEdit.title,
      body: wordEdit.body,
      source: wordEdit.source
    };
    const isValid = await wordSchema.isValid(formData);
    {
      isValid &&
      updateWord(id, wordEdit);
      navigate('/wordsList');
    };
    
  };

  const createChange = (e) => {
    const { name, value } = e.target;
    setWordEdit((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  return (
    
    <form className='form' onSubmit={handleSubmit}>
      <h1>Edit this word</h1>

      <UseInput
        label='Title'
        type="text"
        onChange={createChange}
        value={wordEdit.title}
        name='title'
        required={true}
      />

      <UseInput
        label='Body'
        type="text"
        onChange={createChange}
        value={wordEdit.body ? wordEdit.body : ''}
        name='body'
        required={false}
      />

      <UseInput
        label='Source'
        type="text"
        onChange={createChange}
        value={wordEdit.source ? wordEdit.source : ''}
        name='source'
        required={false}
      />

      <UseButton text='submit' />
      <Link
        to={`/wordsList`}
        className='cancel-btn'
      >
        Cancel
      </Link>
      
      
      {/* <Link to={`/wordsList`} >
        Cancel
      </Link> */}

    </form>

  );
};

export default Edit;


