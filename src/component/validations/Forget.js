import * as yup from "yup";

export const forgetSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
  code: yup.string().required()
});