import * as yup from "yup";

export const wordSchema = yup.object().shape({
  title: yup.string().required(),
  body: yup.string(),
  source: yup.string()
});