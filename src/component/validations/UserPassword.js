import * as yup from "yup";

export const passSchema = yup.object().shape({
  password: yup.string().required(),
  newpass: yup.string().required()
});