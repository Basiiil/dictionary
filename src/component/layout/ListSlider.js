import React from 'react';
import { useNavigate,Link  } from 'react-router-dom';
import './Slider.css';
import { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import cambridge from '../../assets/image/cambridge.png';
import urban from '../../assets/image/urban.png';
import youglish from '../../assets/image/youglish.png';
import oxford from '../../assets/image/oxford.png';
import google from '../../assets/image/google.png';


function ListSlider({ title }) {
  const navigate = useNavigate()
  return (

    <div className="section-slider">
      <Swiper
        slidesPerView={2}
        spaceBetween={20}
        breakpoints={{
          // when window width is >= 640px
          350: {
            // width: 640,
            slidesPerView: 1,
          },
          // when window width is >= 768px
          600: {
            // width: 768,
            slidesPerView: 2,
          },
        }}

        navigation
        modules={[Navigation]}
        className="size"
      >
        <SwiperSlide
          className='section-slider__slide'
        >
          <Link
            to={`/wordsList/cambridge/${title}`}
            target="_blank">
            
            <img src={cambridge}
              alt="cambridge image"
              className='section-slider__slide-image'
            />

          </Link>
        </SwiperSlide>
        <SwiperSlide
          className='section-slider__slide'
        >
          <Link
            to={`/wordsList/urban/${title}`}
            target="_blank">
            
            <img
              src={urban}
              alt="urban image"
              className='section-slider__slide-image'
            />

          </Link>
          
        </SwiperSlide>
        <SwiperSlide
          className='section-slider__slide'
        >
          <a
            href={ `https://youglish.com/pronounce/${title}/english?` }
            target='_blank'
          >

            <img
              src={youglish}
              alt="youglish image"
              className='section-slider__slide-image'
            />

            </a>
        </SwiperSlide>
        <SwiperSlide
          className='section-slider__slide'
        >
          <a
            href={ `https://www.oxfordlearnersdictionaries.com/definition/english/${title}_1?q=${title}` }
            target='_blank'
          >

            <img
              src={oxford}
              alt="oxford image"
              className='section-slider__slide-image'
            />

          </a>
        </SwiperSlide>
        <SwiperSlide
          className='section-slider__slide'
        >
          <a
            href={ `https://www.google.com/search?tbm=isch&q=${title}` }
            target='_blank'
          >

            <img
              src={google}
              alt="google image"
              className='section-slider__slide-image'
            />

          </a>
        </SwiperSlide>
      </Swiper>
    </div>

  )
}

export default ListSlider