import React,{useContext} from 'react';
import AlertContext from '../../context/alert/AlertContext';
import { Alert } from 'react-bootstrap';

const ShowAlert = () => {
  const alertContext = useContext(AlertContext);
  const { alert } = alertContext;
  return (
    alert !== null && (
      <Alert variant={alert.type}>
        {alert.msg}
      </Alert>
    )

  );
};

export default ShowAlert