import React from 'react';
import './Navigator.css';
import profile from '../../assets/image/profile.png';
import { Navbar, Container, Nav } from 'react-bootstrap';

const Navigator = () => {

  if (localStorage.getItem('token')) {
    return (
      // <Navbar bg="light" variant="light">
      //   <Container>
      //     <Navbar.Brand href="#home">Navbar</Navbar.Brand>
      //     <Nav className="me-auto">
      //       <Nav.Link href="/create">Create</Nav.Link>
      //       <Nav.Link href="/wordsList">Words List</Nav.Link>
      //       <Nav.Link href="/" onClick={() => localStorage.clear()} >Log Out</Nav.Link>
      //       <Nav.Link href="/changePass">Change Password</Nav.Link>
      //     </Nav>
      //   </Container>
      // </Navbar>

      <nav className="navbar">
        <div className="navbar__item">
          <img src={profile} alt="profile" className="image" />
          <p>zahra zarei</p>
        </div>
        <div className="navbar__item logo">logo</div>
      </nav>
    )
  } else {
    return (
      // <Navbar bg="light" variant="light">
      //   <Container>
      //     <Navbar.Brand href="#home">Navbar</Navbar.Brand>
      //     <Nav className="me-auto">
      //       <Nav.Link href="/register">Register</Nav.Link>
      //       <Nav.Link href="/login">Login</Nav.Link>
      //     </Nav>
      //   </Container>
      // </Navbar>

      <nav className="navbar">
      <div className="navbar__item">
        <img src={profile} alt="profile" className="image" />
        <p>zahra zarei</p>
      </div>
      <div className="navbar__item logo">logo</div>
    </nav>
    )
  }

};

export default Navigator