import React from 'react';
import './UseButton.css'

function UseButton(props) {
  return (
    <button type='submit'
      className='form__btn'>
      {props.text}
    </button>
  )
}

export default UseButton