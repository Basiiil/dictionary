import React from 'react'
import ReactReadMoreReadLess from "react-read-more-read-less";

function ReadMore({text,limit}) {
  return (
    <ReactReadMoreReadLess
      readMoreClassName='textmore'
      readLessClassName='textmore'
      charLimit={15}
      // charLimit={limit}
      readMoreText={"Read more ▼"}
      readLessText={"Read less ▲"}
    >
      {text}
    </ReactReadMoreReadLess>
  );
}

export default ReadMore
