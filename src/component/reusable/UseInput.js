import React from 'react';
import './UseInput.css'

function UseInput(props) {
  return (
    <div>
      <label
        htmlFor={props.label}
        className="form__label"
      >{props.label}:
      </label>
      <input
        type={props.type}
        placeholder={props.placeholder}
        className='form__input'
        onChange={props.onChange}
        value={props.value}
        name={props.name}
        required={props.required}
      />
    </div> 
  )
}

export default UseInput