import React from 'react';
import { Link  } from 'react-router-dom';
import './UseSocial.css'

function UseSocial(props) {
  return (
    <div>
      
      <h3 className='or-text'>OR</h3>
      
      <div className='form__social'>
        <Link to=''>
          <img src={props.twitter} alt="twitter" className='form__social-icon' />
        </Link>
        <Link to=''>
          <img src={props.facebook} alt="facebook" className='form__social-icon' />
        </Link>
        <Link className='form__social-btn' to=''>
          <img src={props.google} alt="google" className='form__social-icon' />
        </Link>
        
      </div>
    </div>
  )
}

export default UseSocial