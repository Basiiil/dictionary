import './App.css';
import { BrowserRouter as Router, Route,Routes } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigator from './component/layout/Navigator';
import Register from './component/outh/Register';
import Verification from './component/outh/Verification';
import ForgetPassword from './component/outh/ForgetPassword';
import ForgetPassCompletion from './component/outh/ForgetPassCompletion';
import Login from './component/outh/Login';
import ChangePass from './component/outh/ChangePass';
import Create from './component/words/Create';
import Words from './component/words/Words';
import Cambridge from './component/webSearch/Cambridge';
import Urban from './component/webSearch/Urban';
import Edit from './component/words/Edit';
import Show from './component/words/Show';
import ShowAlert from './component/layout/ShowAlert';
import OuthState from './context/outh/OuthState';
import WordState from './context/word/WordState';
import AlertState from './context/alert/AlertState';




function App() {
  return (
    <OuthState>
      <WordState>
        <AlertState>
          <Router>
            <div className="App">
              <Navigator />
              <ShowAlert />
              <Routes>
                
                <Route path='/register' element={<Register />} />
                <Route path='/verification' element={<Verification />} />
                <Route path='/forgetPassword' element={<ForgetPassword />} />
                <Route path='/forgetPassCompletion' element={<ForgetPassCompletion />} />
                <Route path='/login' element={<Login />} />
                <Route path='/changePass' element={<ChangePass />} />
                <Route path='/wordsList' element={<Words />} />
                <Route path='/create' element={<Create />} />
                <Route path='/wordsList/cambridge/:title' element={<Cambridge />} />
                <Route path='/wordsList/urban/:title' element={<Urban />} />
                <Route path='/wordsList/edit/:id' element={<Edit />} />
                <Route path='/wordsList/show/:id' element={<Show />} />
                
              </Routes>
            </div>
          </Router>
        </AlertState>
      </WordState>
    </OuthState>

  );
}

export default App;
